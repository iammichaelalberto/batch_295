--create database simulasiDB
--create table biodata
--(
--id bigint,
--first_name varchar(20) not null,
--last_name varchar(30) not null,
--dob datetime not null,
--pob varchar(50) not null,
--addresss varchar(255) not null,
--gender varchar(1) not null
--)
--select * from biodata
--insert into biodata
--(id,first_name,last_name,dob,pob,addresss,gender) values
--('1','soraya','rahayu','1990-12-22','Bali','Jl. Raya Kuta, Bali','P'),
--('2','hanum','danuary','1990-01-02','Bandung','Jl. Berkah Ramadhan, Bandung','P'),
--('3','melati','marcelia','1991-03-03','Jakarta','Jl. Mawar 3, Brebes','P'),
--('4','farhan','Djokrowidodo','1989-10-11','Jakrata','Jl. Bahari Raya, Solo','L')

--drop table biodata
--create table employee
--(
--id bigint primary key identity (1,1),
--biodata_id bigint not null,
--nip varchar(5) not null,
--statuss varchar(10) not null,
--join_date datetime not null,
--salary decimal(10,0) not null,
--)
--select * from employee
--insert into employee
--(biodata_id,nip,statuss,join_date,salary) values
--('1','XA001','Permanen','2015-11-01 00:00:00.000','12000000'),
--('2','XA002','Kontrak','2017-01-02 00:00:00.000','10000000'),
--('3','XA003','Kontrak','2018-08-19 00:00:00.000','10000000')

--create table contact_person
--(
--id bigint primary key identity (1,1) not null,
--biodata_id bigint not null,
--typee varchar(5) not null,
--contact varchar(100) not null
--)
--select * from contact_person
--insert into contact_person
--(biodata_id,typee,contact) values
--('1','MAIL','soraya.rahayu@gmail.com'),
--('1','PHONE','085612345678'),
--('2','MAIL','hanum.danuary@gmail.com'),
--('2','PHONE','081312345678'),
--('2','PHONE','087812345678'),
--('3','MAIL','melati.marcelia@gmail.com')

--create table leave
--(

--id bigint primary key identity (1,1) not null,
--type varchar (10) not null,
--namee varchar (100) not null
--)
--select * from leave
--insert into leave
--(type,namee) values
--('Regular','Cuti Tahunan'),
--('Khusus','Cuti Menikah'),
--('Khusus','Cuti Haji & Umroh'),
--('Khusus','Cuti Melahirkan')

--create table employee_leave
--(
--id int primary key identity (1,1) not null,
--employee_id int not null,
--periode varchar (4) not null,
--regular_quota int not null
--)
--select * from employee_leave
--insert into employee_leave
--(employee_id,periode,regular_quota) values
--(1,'2021',16),
--(2,'2021',12),
--(3,'2021',12)


--create table request
--(
--id bigint primary key identity (1,1) not null,
--employee_id bigint not null,
--leave_id bigint not null,
--start_date date not null,
--end_date date not null,
--reason varchar (255) not null
--)
--select * from request
--insert into request
--(employee_id,leave_id,start_date,end_date,reason) values
--('1','1','2021-10-10','2021-10-12','Liburan'),
--('1','1','2021-11-12','2021-11-15','Acara keluarga'),
--('2','2','2021-05-05','2021-05-07','Menikah'),
--('2','1','2021-09-09','2021-09-13','Touring'),
--('2','1','2021-12-20','2021-12-23','Acara keluarga')





select * from biodata
select * from employee
select * from contact_person
select * from biodata
select * from leave
select * from employee_leave
select * from request

select * from biodata as bio
left join employee as emp on  bio.id = emp.id 
left join contact_person as con on bio.id = con.biodata_id
left join leave as lea on bio.id = lea.id
left join employee_leave as empi on bio.id = empi.id
--left join request as req on bio.id = req.employee_id

--nomor 1 
select top 1 CONCAT(bio.first_name,' ', bio.last_name) as nama_utuh ,
emp.join_date
 from biodata as bio
left join employee as emp on  bio.id = emp.id 
--left join contact_person as con on bio.id = con.biodata_id
left join leave as lea on bio.id = lea.id
left join employee_leave as empi on bio.id = empi.id
--order by emp.join_date desc

-- 2. Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan.
select  emp.nip,
concat(bio.first_name,' ',bio.last_name) as nama_utuh,
req.start_date,
datediff (day,req.start_date,req.end_date) + 1 as lama_cuti,
req.reason as keterangan
from biodata as bio
left join employee as emp on  bio.id = emp.id		
--left join contact_person as con on bio.id = con.biodata_id
left join leave as lea on bio.id = lea.id
--left join employee_leave as empi on bio.id = empi.id
right join request as req on bio.id = req.employee_id
where day(req.end_date) >=22 and month (req.end_date) =12
--where '2022-12-22' between req.start_date and req.end_date

-- 3. Menampilkan daftar karyawan yang sudah mengajukan cuti lebih dari 2 kali. Tampilkan data berisi no_induk, nama, jumlah pengajuan .
select bio.id, concat(bio.first_name,' ',bio.last_name) as nama_utuh,
count(req.employee_id) from request as req
join biodata as bio on bio.id = req.employee_id
group by bio.id, bio.first_name, bio.last_name
having count(req.employee_id) > 2


-- 4. Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap karyawan tahun ini adalah sesuaidengan quota cuti.
--Daftar berisi no_induk, nama, quota, cuti ygsudah di ambil dan sisa_cuti
select emp.nip,
concat (bio.first_name,' ',bio.last_name)as nama_karyawan,
case
when sum(datediff(day,req.start_date,req.end_date) + 1 ) is NULL then 0
else sum(datediff(day,req.start_date,req.end_date) + 1 )
end as cuti_yg_sudah_diambil,
empi.regular_quota,
case
when empi.regular_quota - sum(datediff(day,req.start_date,req.end_date)+1) is NULL then empi.regular_quota
else empi.regular_quota - sum(datediff(day,req.start_date,req.end_date)+1)
end as sisa_cuti
from biodata as bio
right join employee as emp on emp.biodata_id = bio.id
left join request as req on req.employee_id = bio.id
left join employee_leave as empi on empi.employee_id = bio.id
group by emp.nip,concat (bio.first_name,' ',bio.last_name),empi.regular_quota
--group by emp.nip,concat (bio.first_name,' ',bio.last_name), empi.regular_quota,req.start_date,req.end_date

-- 5. Perusahaan akan meberikan bonus bagi karyawan yang sudah bekerja lebih dari 5 tahun sebanyak 1.5 kali gaji.
--Tampilan No induk, Fullname, Berapa lama bekerja, Bonus, Total Gaji(gaji + bonus)
select bio.id,
concat (bio.first_name,' ',bio.last_name)as nama_karyawan,
datediff(year,emp.join_date,getdate()) as lama_kerja,
case
when datediff(year,emp.join_date,getdate()) >5 then 1.5*emp.salary
else 0
end  as bonus,
case
when datediff(year,emp.join_date,getdate()) >5 then 2.5*emp.salary
else emp.salary
end as total_gaji
from biodata as bio
 join employee as emp on bio.id = emp.biodata_id
group by bio.id,concat (bio.first_name,' ',bio.last_name),emp.join_date,emp.salary

--6. Tampilkan nip, nama_lengkap, jika karyawan ada yg berulang tahun di hari ini akan diberikan hadiah bonus
----sebanyak 5% dari gaji jika tidak ulang tahun maka bonus 0 dan total gaji . Tampilkan No Induk, nama, Tgl lahir , Usia, Bonus, Total Gaji
select emp.nip,
concat (bio.first_name,' ',bio.last_name)as nama_karyawan,
bio.dob as tgl_lahir,
datediff (year,bio.dob,getdate()) as usia,
case
when day(bio.dob) =22  and month(bio.dob) = 12 then 0.05*emp.salary
else 0
end  as bonus,
case
when datediff(year,emp.join_date,getdate()) >5 then 1.05*emp.salary
else emp.salary
end as total_gaji
from biodata as bio
right join employee as emp on bio.id = emp.biodata_id

--7. Tampilkan No Induk, nama, Tgl lahir , Usia. Urutkan biodata dari yg paling muda sampai yg tua

select emp.nip,
concat (bio.first_name,' ',bio.last_name)as nama_karyawan,
bio.dob as tgl_lahir,
datediff (year,bio.dob,getdate()) as usia
from biodata as bio
right join employee as emp on bio.id = emp.biodata_id
order by bio.dob desc

--8. Tampikan Karyawan yg belum pernah Cuti
select concat (bio.first_name,' ',bio.last_name)as nama_karyawan
from biodata as bio
left join employee as empi on empi.biodata_id = bio.id  
left join request as req on req.employee_id= empi.id
where empi.biodata_id is not null and req.employee_id  is null
select* from request

--9. Tampikan Nama Lengkap, Jenis Cuti, Durasi Cuti, dan no telp yang sedang cuti
select concat (bio.first_name,' ',bio.last_name)as nama_karyawan,
lea.namee as jenis_cuti,
datediff(day,req.start_date,req.end_date) as durasi_cuti,
con.contact
 from biodata as bio
 join employee as emp on bio.id = emp.biodata_id
 join contact_person as con on con.biodata_id = emp.biodata_id
 --join leave as lea on lea.id = con.biodata_id
 join employee_leave as empi on empi.employee_id =con.biodata_id
 join request as req  on req.employee_id = empi.employee_id
 join leave as lea on lea.id = req.leave_id
where con.typee='PHONE' and req.start_date between '2021-12-20' and '2021-12-23'
--group by  lea.type,concat (bio.first_name,' ',bio.last_name), con.typee , con.contact , datediff(day,req.start_date,req.end_date)
select * from request
select * from leave
select lea.id, concat (bio.first_name,' ',bio.last_name)as nama_karyawan from request as req
right join leave as lea on req.leave_id = lea.id
right join biodata as bio on bio.id = lea.id
group by lea.id, concat (bio.first_name,' ',bio.last_name)
--10. Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan
select emp.biodata_id,
concat (bio.first_name,' ',bio.last_name)as nama_karyawan
from biodata as bio
left join employee as emp on emp.biodata_id=bio.id
where biodata_id is null

--11. buatlah sebuah view yg menampilkan data nama lengkap, tgl lahir dan tmpat lahir , status, dan salary --belum paham view

create view terserah_data as
select
concat (bio.first_name,' ',bio.last_name)as nama_karyawan,
bio.dob,
bio.pob,
emp.statuss,
emp.salary
from biodata as bio
join employee as emp on emp.biodata_id = bio.id

select * from terserah_data


--MAju 4. Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap
--karyawan tahun ini adalah sesuaidengan quota cuti.
--Daftar berisi no_induk, nama, quota, cuti ygsudah di ambil dan sisa_cuti
select emp.nip,
concat(bio.first_name,' ',bio.last_name) as nama_karyawan,
empi.regular_quota,
case
when sum (datediff(day,req.start_date,req.end_date)+1) is null then 0
else sum (datediff(day,req.start_date,req.end_date)+1)
end as cuti_yg_sudah_diambil,
case
when empi.regular_quota - sum (datediff(day,req.start_date,req.end_date)+1) is null then empi.regular_quota
else empi.regular_quota - sum (datediff(day,req.start_date,req.end_date)+1)
end as sisa_cuti
from biodata as bio
right join employee as emp on emp.biodata_id = bio.id
left join request as req on req.employee_id = bio.id
left join employee_leave as empi on empi.employee_id = bio.id
group by emp.nip,bio.first_name,bio.last_name,empi.regular_quota
