﻿using System;
using System.Text;

namespace PRDay6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /* string[] input = "A b 1".Split(" ");
             Console.Write($"{byte(9)}");*/

            string value = "Abc3!a";

            // Convert the string into a byte[].
            byte[] asciiBytes = Encoding.ASCII.GetBytes(value);
            /*Console.Write(asciiBytes[0]+ " ");*/

            var str = System.Text.Encoding.Default.GetString(asciiBytes);
            /*Console.Write(str+" ");*/
            string result = System.Text.Encoding.UTF8.GetString(asciiBytes);
            /*Console.Write(result);*/
            /*Console.Write(asciiBytes.Length);*/
            int kapital = 0;
            int hurufKecil = 0;
            int angka = 0;
            int karakterKhusus = 0;
            for (int i = 0; i < asciiBytes.Length; i++)
            {
                if (asciiBytes[i]>=65 && asciiBytes[i] <= 90)   //65 sampai 90 itu huruF KAPITAL
                {
                    kapital++;
                }
                else if(asciiBytes[i] >= 97 && asciiBytes[i] <= 122)    //97 sampai 122 huruf kecil
                {
                    hurufKecil++;
                }
                else if(asciiBytes[i] >= 48 && asciiBytes[i] <= 57)       //48 sampai 57 angka(0 sampai 9)
                {
                    angka++;
                }
                else if((asciiBytes[i] >= 35 && asciiBytes[i] <= 38) || asciiBytes[i]==33 || asciiBytes[i] == 64 || asciiBytes[i] == 94|| (asciiBytes[i] >= 40 && asciiBytes[i] <= 43) || asciiBytes[i]==45)         // 35 sampai 28 itu # $ % & //33 itu ! // 64 itu @
                {
                    karakterKhusus++;                                                                                   //95 itu ^////45 itu -// 40 sampai 43 itu ( ) * +
                }


            }
            if (value.Length < 5)
            {
                Console.Write($"Pesan kurang panjang pa, serius lah pa");
                Console.Write("\n");
            }
            if (angka <=0)
            {
                Console.Write($"fasword anda tidak ada angka pa,plese deh ");
                Console.Write("\n");
            }
            if (kapital <= 0)
            {
                Console.Write($"fasword anda tidak ada huruf besar pa,plese deh ");
                Console.Write("\n");
            }
            if (hurufKecil <= 0)
            {
                Console.Write($"fasword anda tidak ada huruf kecil pa, ayolah ");
                Console.Write("\n");
            }
            if (karakterKhusus <= 0)
            {
                Console.Write($"fasword anda tidak ada karakter khusus pa, dahlah ");
                Console.Write("\n");
            }
            else
                Console.Write($"Password sudah seterong ");
        }
    }
}
