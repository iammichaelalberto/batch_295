﻿using System;

namespace AppleAndOrangeTerUpDate
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("AppleDanOrange\nTentukan Batas Bawah Rumah Sam (s)\nTentukan Batas Atas Rumah Sam (t)\na itu pohon apel disebelah kiri rumah sam \nb itu pohon jeruk disebelah kanan rumah sam");
            Console.WriteLine("Tentukan posisi pohon apel ke rumah sam (a)\nTentukan posisi pohon jeruk ke rumah sam (b)\nx merupakan jumlah apel yang jatuh beserta arahnya \ny merupakan jumlah jeruk yang jatuh beserta arahnya");
            Console.WriteLine("Negatif menandakan bahwa apel tersebut jatuh ke kiri pohon\nContoh inputan x atau y : 6,-9,6,-7\nDengan Syarat a < s < t < b\n----------------------------------------------------------");

            Console.Write("s = ");
            int s = int.Parse(Console.ReadLine());
            Console.Write("t = ");
            int t = int.Parse(Console.ReadLine());
            Console.Write("a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("b = ");
            int b = int.Parse(Console.ReadLine());
            Console.Write("x = ");
            int[] x = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("y = ");
            int[] y = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            //int s, a, t, b;
            //s = 7; a = 5;
            //t = 11; b = 15;
            //int[] x = Array.ConvertAll("-2,2,1".Split(","), int.Parse);
            //int[] y = Array.ConvertAll("5,-6".Split(","), int.Parse);
            /*Console.WriteLine(x[0]);*/
            int appleJatuhOnPoint = 0;
            int orangeJatuhOnPoint = 0;

            for (int i = 0; i < x.Length; i++)
            {


                if (x[i] >= 0)
                {
                    int z = a + x[i];
                    if (z >= s && z <= t)
                    {
                        appleJatuhOnPoint++;
                    }
                }

            }

            for (int j = 0; j < y.Length; j++)
            {

                if (y[j] <= 0)
                {
                    int zz = b + y[j];
                    if (zz <= t && zz >= s)
                    {
                        orangeJatuhOnPoint++;
                    }
                }

            }
            Console.WriteLine($"apel yang jatuh ke rumah sam sebanyak {appleJatuhOnPoint} buah");
            Console.WriteLine($"jeruk yang jatuh ke rumah sam sebanyak {orangeJatuhOnPoint} buah");
        }
    }
}
